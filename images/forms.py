from django import forms
from urllib import request
from django.core.files.base import ContentFile
from django.utils.text import slugify

from .models import Image


class ImageCreateForm(forms.ModelForm):
    """Users will not enter the image URL directly in
    the form. Instead, we will provide them with a JavaScript tool to
    choose an image from an external site, and our form will receive its
    URL as a parameter. We override the default widget of the url field
    to use a HiddenInput widget. This widget is rendered as an HTML input
    element with a type="hidden" attribute. We use this widget because we
    don't want this field to be visible to users.s.280"""
    class Meta:
        model = Image
        fields = ('title', 'url', 'description', )
        widgets = {
            'url': forms.HiddenInput,
        }

        # the clean_<fieldname>() notation.
        def clean_url(self):
            url = self.cleaned_data['url']  # accessing the dictionary of the form instance.
            valid_extensions = ['jpg', 'jpeg']
            extension = url.rsplit('.', 1)[1].lower()
            print(f'def clean_url({self})extension:{extension}, cleaned_data: {self.cleaned_data}')
            if extension not in valid_extensions:
                raise forms.ValidationError('The given URL does not match valid image extensions.')
            return url

        def save(self, force_insert=False, force_update=False, commit=True):
            """1. We create a new image instance by calling the save() method of
                    the form with commit=False.
                2. We get the URL from the cleaned_data dictionary of the form.
                3. We generate the image name by combining the image title slug
                    with the original file extension.
                4. We use the Python urllib module to download the image and
                    then we call the save() method of the image field, passing it a
                    ContentFile object that is instantiated with the downloaded file
                    content. In this way, we save the file to the media directory
                    of our project. We also pass the save=False parameter to avoid
                    saving the object to the database, yet.
                5. In order to maintain the same behavior as the save() method
                    we override, we save the form to the database only when the
                    commit parameter is True."""
            image = super(ImageCreateForm, self).save(commit=False)

            image_url = self.cleaned_data['url']
            image_name = '{}.{}'.format(slugify(image.title),
                                        image_url.rsplit('.', 1)[1].lower())
            # download image from the given URL
            response = request.urlopen(image_url)
            image.image.save(image_name,
                             ContentFile(response.read()),
                             save=False)
            if commit:
                image.save()
            return image