from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from .forms import ImageCreateForm


@login_required
def image_create(request):
    """
    1. We expect initial data via GET in order to create an instance of
        the form. This data will consist of the url and title attributes
        of an image from an external website and will be provided
        via GET by the JavaScript tool we will create later. For now,
        we just assume that this data will be there initially.
    2. If the form is submitted, we check whether it is valid. If the
        form data is valid, we create a new Image instance, but prevent
        the object from being saved to the database yet by passing
        commit=False to the form's save() method.
    3. We assign the current user to the new image object. This is
        how we can know who uploaded each image.
    4. We save the image object to the database.
    5. Finally, we create a success message using the Django
        messaging framework and redirect the user to the canonical
        URL of the new image. We haven't yet implemented the
        get_absolute_url() method of the Image model; we will do that
        later.
    """
    if request.method == 'POST':
        # form is sent
        form = ImageCreateForm(data=request.POST)
        if form.is_valid():
            # form data is valid
            cd = form.cleaned_data
            new_item = form.save(commit=False)
            # assign current user to the item
            new_item.user = request.user
            new_item.save()
            messages.success(request, 'Image added successfully')
            # redirect to new created item detail view
            return redirect(new_item.get_absolute_url())
    else:
        # build form with data provided by the bookmarklet via GET
        form = ImageCreateForm(data=request.GET)
    return render(request,
                  'images/image/create.html',
                  {'section': 'images',
                   'form': form})